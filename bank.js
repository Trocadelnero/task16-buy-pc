//Developer wants to buy a computer.

//HTML References + associated Eventlistener
const eleBankBalance = document.getElementById("bank-balance");

const eleSelectPC = document.getElementById("selectPC");
eleSelectPC.addEventListener("change", selectedPC);

const eleBtnDoWork = document.getElementById("btn-work");
eleBtnDoWork.addEventListener("click", workForMoney);

const eleGetLoan = document.getElementById("btn-get-loan");
eleGetLoan.addEventListener("click", getLoan);

const eleBtnBuyPC = document.getElementById("btn-buy");
eleBtnBuyPC.addEventListener("click", buyPC);

//Variables
const HOURLY_RATE = 10;
const MIN_BALANCE_FOR_LOAN = 999;
const LOAN_LIMIT = 1;

let bankBalance;
let pcCost;
let loansGiven;
let loanSize;

//Set some variables on page load.
function setStart() {
  console.log("Good day to buy a PC");

  bankBalance = parseFloat(eleBankBalance.innerText);
  pcCost = parseInt(eleSelectPC.value);
  loanSize = 0;
  loansGiven = 0;
}

//Function -> "Work for money"-button
function workForMoney() {
  bankBalance += HOURLY_RATE;
  eleBankBalance.innerText = bankBalance;
}

// Selected a PC.
function selectedPC() {
  // this -> refers to the selectedPC
  pcCost = parseInt(this.value);
  console.log(`Selected: + ${pcCost}`);
}
//"Get Loan"- Related
function getLoan() {
  //Only one loan allowed
  if (loansGiven >= LOAN_LIMIT) {
    alert("No can do, sir! One loan is enough!, you know that already");
    return;
  }
  // bankBalance need to meet min requirement for loan.
  if (bankBalance < MIN_BALANCE_FOR_LOAN) {
    alert("Sorry, you need more money to get a loan!");
    return;
  }
  // Get a loan for selectedPC
  const acceptLoan = confirm(
    `You are about to get a loan for ${pcCost} Kr. Are you sure you want it?😈`
  );
  console.log(acceptLoan);

  if (acceptLoan === true) {
    console.log("You have accepted the loan...");
    //Update loansGiven + bankBalance
    loansGiven += 1;
    bankBalance += pcCost;
    eleBankBalance.innerText = bankBalance;
  }
}
//Buy a PC and update variables, or get "banned"
function buyPC() {
  if (bankBalance >= pcCost) {
    bankBalance -= pcCost;
    eleBankBalance.innerText = bankBalance;
    alert(`Nice! you now own a PC for ${pcCost} Kr`);
  } else {
    alert(
      "Stores stares at you 😡... Sorry, you are now banned from this store."
    );
  }
}
